"use strict";

const degree = 360;

function clock() {
  const currentTime = new Date();
  const hour = currentTime.getHours();
  const minute = currentTime.getMinutes();
  const second = currentTime.getSeconds();

  const hourDegree = degree * (hour / 12);
  const minuteDegree = degree * (minute / 60);
  const secondDegree = degree * (second / 60);

  document.getElementById("hour").style.transform =
    "rotate(" + hourDegree + "deg)";
  document.getElementById("min").style.transform =
    "rotate(" + minuteDegree + "deg)";
  document.getElementById("sec").style.transform =
    "rotate(" + secondDegree + "deg)";
}

setInterval(function () {
  clock();
}, 1000);
